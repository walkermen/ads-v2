<?php
/**
 * Created by PhpStorm.
 * User: dongyanan
 * Date: 2019/8/6
 * Time: 5:01 PM
 */

namespace AmazonAds\Laravel\Advertising;

use Illuminate\Support\Facades\Log;

class AmzAdsClinet
{
    private $config = array(
        "clientId" => null,
        "clientSecret" => null,
        "region" => null,
        "accessToken" => null,
        "refreshToken" => null,
        "expires_at" => null,
        "sandbox" => false
    );

    private $apiVersion = null;
    private $applicationVersion = null;
    private $userAgent = null;
    private $endpoint = null;
    private $tokenUrl = null;
    private $requestId = null;
    private $endpoints = null;
    private $versionStrings = null;

    public $profileId = null;

    public function __construct($config)
    {
        $this->endpoints = config('amz-ads.endpoints');

        $this->versionStrings = config('amz-ads.versionStrings');

        $this->apiVersion = $this->versionStrings["apiVersion"];
        $this->applicationVersion = $this->versionStrings["applicationVersion"];
        $this->userAgent = "AdvertisingAPI PHP Client Library v{$this->applicationVersion}";

        $this->_validateConfig($config);
        $this->_validateConfigParameters();
        $this->_setEndpoints();

    }

    public function doRefreshToken()
    {
        if ( (is_null($this->config["accessToken"]) && !is_null($this->config["refreshToken"])) || ($this->config["expires_at"] < time() + 600) ) {
            /* convenience */
            $headers = array(
                "Content-Type: application/x-www-form-urlencoded;charset=UTF-8",
                "User-Agent: {$this->userAgent}"
            );

            $refresh_token = rawurldecode($this->config["refreshToken"]);

            $params = array(
                "grant_type" => "refresh_token",
                "refresh_token" => $refresh_token,
                "client_id" => $this->config["clientId"],
                "client_secret" => $this->config["clientSecret"]);

            $data = "";
            foreach ($params as $k => $v) {
                $data .= "{$k}=".rawurlencode($v)."&";
            }

            $url = "https://{$this->tokenUrl}";

            $request = new CurlRequest();
            $request->setOption(CURLOPT_URL, $url);
            $request->setOption(CURLOPT_HTTPHEADER, $headers);
            $request->setOption(CURLOPT_USERAGENT, $this->userAgent);
            $request->setOption(CURLOPT_POST, true);
            $request->setOption(CURLOPT_POSTFIELDS, rtrim($data, "&"));

            $response = $this->_executeRequest($request);

            $response_array = json_decode($response["response"], true);
            if (array_key_exists("access_token", $response_array)) {
                $this->config["accessToken"] = $response_array["access_token"];
            } else {
                $this->_logAndThrow("Unable to refresh token. 'access_token' not found in response. ". print_r($response, true));
            }

            return $response;
        }

        return '';
    }

    //获取配置信息
    public function listProfiles()
    {
        return $this->_operation("profiles");
    }

    //在沙箱中注册配置文件,如果对生产端点进行此调用,您将收到一个错误
    public function registerProfile($data)
    {
        return $this->_operation("profiles/register", $data, "PUT");
    }

    //在沙箱中注册品牌。如果对生产端点进行此调用，您将收到一个错误
    public function registerBrand($data)
    {
        return $this->_operation("profiles/registerBrand",$data,"PUT");
    }

    //通过ID检索单个配置文件
    public function getProfile($profileId)
    {
        return $this->_operation("profiles/{$profileId}");
    }

    //更新一个或多个配置文件。广告商使用其profileId进行标识
    public function updateProfiles($data)
    {
        return $this->_operation("profiles", $data, "PUT");
    }

    //广告组合
    public function listPortfolios($data = array())
    {
        return $this->_operation("portfolios", $data);
    }

    //广告组合
    public function listPortfoliosEx($data = array())
    {
        return $this->_operation("portfolios/extended", $data);
    }

    //获取指定广告组合
    public function getPortfolio($portfolioId)
    {
        return $this->_operation("portfolios/{$portfolioId}");
    }

    //获取指定广告组合
    public function getPortfolioEx($portfolioId)
    {
        return $this->_operation("portfolios/extended/{$portfolioId}");
    }

    //创建广告组合
    //$data = array('name' => '','budget' => '','state' => '');
    public function createPortfolios($data)
    {
        return $this->_operation("portfolios",$data,"POST");
    }

    //更新广告组合
    //$data = array('name' => '','budget' => '','state' => '');
    public function updatePortfolios($data)
    {
        return $this->_operation("portfolios",$data,"PUT");
    }

    //通过campaignId检索广告系列。请注意，此调用返回的广告系列字段最少，但效率更高
    public function getCampaign($campaignId, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/campaigns/{$campaignId}");
    }

    //(仅适用产品广告)通过检索广告系列及其扩展字段。请注意，此调用返回的是完整的广告系列字段集（包括投放状态和其他只读字段），但效率不如getCampaign
    public function getCampaignEx($campaignId)
    {
        return $this->_operation("sp/campaigns/extended/{$campaignId}");
    }

    //创建一个或多个广告系列。成功创建的广告系列将被分配一个唯一的
    public function createCampaigns($data)
    {
        return $this->_operation("sp/campaigns", $data, "POST");
    }

    //更新一个或多个广告系列
    public function updateCampaigns($data, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/campaigns", $data, "PUT");
    }

    //将广告活动状态设置为已存档。无法再次激活已归档的实体
    public function archiveCampaign($campaignId, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/campaigns/{$campaignId}", null, "DELETE");
    }

    //商品广告或品牌广告 列表
    public function listCampaigns($data = null, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/campaigns", $data);
    }

    //检索包含扩展字段满足可选过滤条件的“赞助商产品”广告系列的列表
    public function listCampaignsEx($data = null)
    {
        return $this->_operation("sp/campaigns/extended", $data);
    }

    //按ID检索广告组。请注意，此调用返回的是广告组字段的最小集合，但效率更高
    public function getAdGroup($adGroupId)
    {
        return $this->_operation("sp/adGroups/{$adGroupId}");
    }

    //按ID检索广告组及其扩展字段。请注意，此调用会返回完整的广告组字段集（包括投放状态和其他只读字段），但效率不如getAdGroup
    public function getAdGroupEx($adGroupId)
    {
        return $this->_operation("sp/adGroups/extended/{$adGroupId}");
    }

    //创建一个或多个广告组
    public function createAdGroups($data)
    {
        return $this->_operation("sp/adGroups", $data, "POST");
    }

    //更新一个或多个广告组
    public function updateAdGroups($data)
    {
        return $this->_operation("sp/adGroups", $data, "PUT");
    }

    //将广告组状态设置为已存档
    public function archiveAdGroup($adGroupId)
    {
        return $this->_operation("sp/adGroups/{$adGroupId}", null, "DELETE");
    }

    //检索满足可选条件的广告组列表
    public function listAdGroups($data = null)
    {
        return $this->_operation("sp/adGroups", $data);
    }

    //检索具有扩展字段且符合可选过滤条件的广告组列表
    public function listAdGroupsEx($data = null)
    {
        return $this->_operation("sp/adGroups/extended", $data);
    }

    //按ID检索关键字。请注意，此调用返回的是关键字字段的最小集合，但效率高于getBiddableKeywordEx
    public function getBiddableKeyword($keywordId, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/keywords/{$keywordId}");
    }

    //通过ID检索关键字及其扩展字段。请注意，此调用返回完整的关键字字段集（包括服务状态和其他只读字段），但效率不如getBiddableKeyword 赞助商品牌没有扩展的关键字界面
    public function getBiddableKeywordEx($keywordId)
    {
        return $this->_operation("sp/keywords/extended/{$keywordId}");
    }

    //创建一个或多个关键字
    public function createBiddableKeywords($data, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/keywords", $data, "POST");
    }

    //更新一个或多个关键字。当关键字处于待处理状态时，您只能更新出价或对其进行存档。您无法从更改状态
    public function updateBiddableKeywords($data, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/keywords", $data, "PUT");
    }

    public function archiveBiddableKeyword($keywordId, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/keywords/{$keywordId}", null, "DELETE");
    }

    public function listBiddableKeywords($data = null)
    {
        return $this->_operation("sp/keywords", $data);
    }

    public function listBiddableKeywordsEx($data = null)
    {
        return $this->_operation("sp/keywords/extended", $data);
    }

    //获取否定关键字
    public function getNegativeKeyword($keywordId, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/negativeKeywords/{$keywordId}");
    }

    //通过ID检索关键字及其扩展字段。请注意，此调用返回完整的关键字字段集（包括服务状态和其他只读字段），但效率不如getNegativeKeyword
    public function getNegativeKeywordEx($keywordId)
    {
        return $this->_operation("sp/negativeKeywords/extended/{$keywordId}");
    }

    //创建一个或多个否定关键字
    public function createNegativeKeywords($data, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/negativeKeywords", $data, "POST");
    }

    //更新一个或多个否定关键字
    public function updateNegativeKeywords($data, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/negativeKeywords", $data, "PUT");
    }

    //
    public function archiveNegativeKeyword($keywordId, $requestType = "sp")
    {
        return $this->_operation("{$requestType}/negativeKeywords/{$keywordId}", null, "DELETE");
    }

    //检索满足可选条件的否定关键字列表
    public function listNegativeKeywords($data = null)
    {
        return $this->_operation("sp/negativeKeywords", $data);
    }

    //检索包含扩展字段满足可选条件的否定关键字列表。请注意，这仅适用于赞助产品
    public function listNegativeKeywordsEx($data = null)
    {
        return $this->_operation("sp/negativeKeywords/extended", $data);
    }

    //按ID检索广告系列级否定关键字。不适用于供应商
    public function getCampaignNegativeKeyword($keywordId)
    {
        return $this->_operation("sp/campaignNegativeKeywords/{$keywordId}");
    }

    //根据ID检索广告系列级否定关键字以及其他属性
    public function getCampaignNegativeKeywordEx($keywordId)
    {
        return $this->_operation("sp/campaignNegativeKeywords/extended/{$keywordId}");
    }

    //创建一个或多个广告系列否定关键字
    public function createCampaignNegativeKeywords($data)
    {
        return $this->_operation("sp/campaignNegativeKeywords", $data, "POST");
    }

    //更新一个或多个广告系列否定关键字
    public function updateCampaignNegativeKeywords($data)
    {
        return $this->_operation("sp/campaignNegativeKeywords", $data, "PUT");
    }

    //将广告系列的否定关键字状态设置为已删除
    public function archiveCampaignNegativeKeyword($keywordId)
    {
        return $this->_operation("sp/campaignNegativeKeywords/{$keywordId}", null, "DELETE");
    }

    //检索满足可选条件的否定关键字列表
    public function listCampaignNegativeKeywords($data = null)
    {
        return $this->_operation("sp/campaignNegativeKeywords", $data);
    }

    //检索带有否定字段的扩展字段的广告系列否定关键字列表
    public function listCampaignNegativeKeywordsEx($data = null)
    {
        return $this->_operation("sp/campaignNegativeKeywords/extended", $data);
    }

    //请求指定广告组的建议关键字
    public function getAdGroupSuggestedKeywords($adGroupId)
    {
        return $this->_operation("sp/adGroups/{$adGroupId}/suggested/keywords");
    }

    //按ID检索广告系列否定关键字及其扩展字段。请注意，此调用返回完整的关键字字段集（包括服务状态和其他只读字段
    public function getAdGroupSuggestedKeywordsEx($adGroupId)
    {
        return $this->_operation("sp/adGroups/{$adGroupId}/suggested/keywords/extended");
    }

    //提供指定ASIN的建议关键字
    public function getAsinSuggestedKeywords($asin)
    {
        return $this->_operation("sp/asins/{$asin}/suggested/keywords");
    }

    //为指定的ASIN列表提供关键字建议
    public function bulkGetAsinSuggestedKeywords($data)
    {
        return $this->_operation("sp/asins/suggested/keywords",$data,'POST');
    }

    //通过ID检索产品广告
    public function getProductAd($productAdId)
    {
        return $this->_operation("sp/productAds/{$productAdId}");
    }

    //通过ID检索产品广告以及其他属性
    public function getProductAdEx($productAdId)
    {
        return $this->_operation("sp/productAds/extended/{$productAdId}");
    }

    //制作一个或多个产品广告
    public function createProductAds($data)
    {
        return $this->_operation("sp/productAds", $data, "POST");
    }

    //更新一个或多个产品广告
    public function updateProductAds($data)
    {
        return $this->_operation("sp/productAds", $data, "PUT");
    }

    //将产品广告状态设置为已存档
    public function archiveProductAd($productAdId)
    {
        return $this->_operation("sp/productAds/{$productAdId}", null, "DELETE");
    }

    //检索满足可选条件的产品广告列表
    public function listProductAds($data = null)
    {
        return $this->_operation("sp/productAds", $data);
    }

    //检索具有扩展字段满足可选条件的产品广告的列表
    public function listProductAdsEx($data = null)
    {
        return $this->_operation("sp/productAds/extended", $data);
    }

    //检索具有特定目标ID的定位子句
    public function getTargetingClause($targetId)
    {
        return $this->_operation("sp/targets/{$targetId}");
    }

    //检索具有扩展字段满足可选条件的产品广告的列表。
    public function getTargetingClauseEx($targetId)
    {
        return $this->_operation("sp/targets/extended/{$targetId}");
    }

    //检索满足可选条件的产品广告列表
    public function listTargetingClauses()
    {
        return $this->_operation("sp/targets");
    }

    //检索具有扩展属性的定位子句的列表。
    public function listTargetingClausesEx()
    {
        return $this->_operation("sp/targets/extended");
    }

    //创建一个或多个定位表达式。
    public function createTargetingClauses($data)
    {
        return $this->_operation("sp/targets",$data,"POST");
    }

    //更新一个或多个定位条款。
    public function updateTargetingClauses($data)
    {
        return $this->_operation("sp/targets",$data,"PUT");
    }

    //将定位条款的状态设置为已归档
    public function archiveTargetingClause($targetId)
    {
        return $this->_operation("sp/targets/{$targetId}",'',"DELETE");
    }

    //根据输入的ASIN生成要定位的推荐产品列表
    public function createTargetRecommendations($data)
    {
        return $this->_operation("sp/targets/productRecommendations",$data,"POST");
    }

    //获取定位类别列表。
    public function getTargetingCategories()
    {
        return $this->_operation("sp/targets/categories");
    }

    //获得针对单个类别的优化。类别和优化取决于请求中配置文件的marketplaceId
    public function getRefinementsForCategory()
    {
        return $this->_operation("sp/targets/categories/refinements");
    }

    //获取推荐产品的推荐品牌
    public function getBrandRecommendations()
    {
        return $this->_operation("sp/targets/brands");
    }

    //通过获取特定的否定定位条款
    public function getNegativeTargetingClause($targetId)
    {
        return $this->_operation("sp/negativeTargets/{$targetId}");
    }

    //使用特定的目标ID检索带有其他属性的否定目标子句。
    public function getNegativeTargetingClauseEx($targetId)
    {
        return $this->_operation("sp/negativeTargets/extended/{$targetId}");
    }

    //在广告系列一级创建否定定位条款。
    public function createNegativeTargetingClauses($data)
    {
        return $this->_operation("sp/negativeTargets",$data,"POST");
    }

    //检索否定定位子句的列表。
    public function listNegativeTargetingClauses()
    {
        return $this->_operation("sp/negativeTargets");
    }

    //检索具有扩展属性的定位子句的列表。。
    public function listNegativeTargetingClausesEx()
    {
        return $this->_operation("sp/negativeTargets/extended");
    }

    //存档否定定位条款。
    public function archiveNegativeTargetingClause($targetId)
    {
        return $this->_operation("sp/negativeTargets/{$targetId}","","DELETE");
    }

    //更新否定定位条款。
    public function updateNegativeTargetingClauses($data)
    {
        return $this->_operation("sp/negativeTargets}",$data,"PUT");
    }

    //检索指定的出价建议数据
    public function getAdGroupBidRecommendations($adGroupId)
    {
        return $this->_operation("sp/adGroups/{$adGroupId}/bidRecommendations");
    }

    //检索指定关键字ID的出价建议数据
    public function getKeywordBidRecommendations($keywordId)
    {
        return $this->_operation("sp/keywords/{$keywordId}/bidRecommendations");
    }

    //检索一个或多个关键字的关键字出价推荐数据
    /*{
        "adGroupId": 264272438240075,
        "keywords": [
            {"keyword": "keyword one","matchType": "broad"},
            {"keyword": "keyword two","matchType": "broad"}
        ]
    }*/
    public function createKeywordBidRecommendations($adGroupId, $keywordsId)
    {
        $data = array(
            "adGroupId" => $adGroupId,
            "keywords" => $keywordsId);
        return $this->_operation("sp/keywords/bidRecommendations", $data, "POST");
    }

    //检索关键字，产品或自动定位表达式的出价建议列表(表达式必须是全关键字，产品或自动定位，不能混合使用）
    public function getBidRecommendations($adGroupId, $expressions)
    {
        $data = array(
            "adGroupId" => $adGroupId,
            "expressions" => $expressions);
        return $this->_operation("sp/targets/bidRecommendations", $data, "POST");
    }

    //Update the value of the bid multiplier
    public function updateCampaignAdGroup($data)
    {
        return $this->_operation("hsa/campaigns", $data, "PUT");
    }

    //请求有关赞助产品或赞助品牌的单个记录类型的所有实体的快照报告。
    public function requestSnapshot($recordType, $data = null, $requestType = 'sp')
    {
        return $this->_operation("{$requestType}/{$recordType}/snapshot", $data, "POST");
    }

    public function getSnapshot($snapshotId)
    {
        $req = $this->_operation("snapshots/{$snapshotId}");
        if ($req["success"]) {
            $json = json_decode($req["response"], true);
            if ($json["status"] == "SUCCESS") {
                return $this->_download($json["location"]);
            }
        }
        return $req;
    }

    //检索给定广告商的商店列表。
    public function listStores()
    {
        return $this->_operation("stores");
    }

    //
    public function getStore($brandEntityId)
    {
        return $this->_operation("stores/{$brandEntityId}");
    }

    //获取赞助商品或赞助品牌的报告信息
    public function requestReport($recordType, $data = null, $requestType = 'sp')
    {
        return $this->_operation("{$requestType}/{$recordType}/report", $data, "POST");
    }

    //获取报告
    public function getReport($reportId)
    {
        $req = $this->_operation("reports/{$reportId}");
        if ($req["success"]) {
            $json = json_decode($req["response"], true);
            if ($json["status"] == "SUCCESS") {
                return $this->_download($json["location"]);
            }
        }
        return $req;
    }

    private function _download($location, $gunzip = false)
    {
        $headers = array();

        if (!$gunzip) {
            /* only send authorization header when not downloading actual file */
            array_push($headers, "Authorization: bearer {$this->config["accessToken"]}");
        }

        if (!is_null($this->profileId)) {
            array_push($headers, "Amazon-Advertising-API-Scope: {$this->profileId}");
        }

        $request = new CurlRequest();
        $request->setOption(CURLOPT_URL, $location);
        $request->setOption(CURLOPT_HTTPHEADER, $headers);
        $request->setOption(CURLOPT_USERAGENT, $this->userAgent);

        if ($gunzip) {
            $response = $this->_executeRequest($request);
            $response["response"] = gzdecode($response["response"]);
            return $response;
        }

        return $this->_executeRequest($request);
    }

    private function _operation($interface, $params = array(), $method = "GET")
    {
        $headers = array(
            "Amazon-Advertising-API-ClientId: {$this->config["clientId"]}",
            "Authorization: bearer {$this->config["accessToken"]}",
            "Content-Type: application/json",
            "User-Agent: {$this->userAgent}"
        );

        if (!is_null($this->profileId)) {
            array_push($headers, "Amazon-Advertising-API-Scope: {$this->profileId}");
        }

        $request = new CurlRequest();
        $url = "{$this->endpoint}/{$interface}";
        $this->requestId = null;
        $data = "";

        switch (strtolower($method)) {
            case "get":
                if (!empty($params)) {
                    $url .= "?";
                    foreach ($params as $k => $v) {
                        $url .= "{$k}=".rawurlencode($v)."&";
                    }
                    $url = rtrim($url, "&");
                }
                break;
            case "put":
            case "post":
            case "delete":
                if (!empty($params)) {
                    $data = json_encode($params);
                    $request->setOption(CURLOPT_POST, true);
                    $request->setOption(CURLOPT_POSTFIELDS, $data);
                }
                break;
            default:
                $this->_logAndThrow("Unknown verb {$method}.");
        }

        $request->setOption(CURLOPT_URL, $url);
        $request->setOption(CURLOPT_HTTPHEADER, $headers);
        $request->setOption(CURLOPT_USERAGENT, $this->userAgent);
        $request->setOption(CURLOPT_CUSTOMREQUEST, strtoupper($method));
        return $this->_executeRequest($request);
    }

    protected function _executeRequest(CurlRequest $request)
    {
        $response = $request->execute();
        $this->requestId = $request->requestId;
        $response_info = $request->getInfo();
        $request->close();

        if ($response_info["http_code"] == 307) {
            /* application/octet-stream */
            return $this->_download($response_info["redirect_url"], true);
        }

        if (!preg_match("/^(2|3)\d{2}$/", $response_info["http_code"])) {
            $requestId = 0;
            $json = json_decode($response, true);
            if (!is_null($json)) {
                if (array_key_exists("requestId", $json)) {
                    $requestId = json_decode($response, true)["requestId"];
                }
            }
            return array("success" => false,
                "code" => $response_info["http_code"],
                "response" => $response,
                "requestId" => $requestId);
        } else {
            return array("success" => true,
                "code" => $response_info["http_code"],
                "response" => $response,
                "requestId" => $this->requestId);
        }
    }

    private function _validateConfig($config)
    {
        if (is_null($config)) {
            $this->_logAndThrow("'config' cannot be null.");
        }

        foreach ($config as $k => $v) {
            if (array_key_exists($k, $this->config)) {
                $this->config[$k] = $v;
            } else {
                $this->_logAndThrow("Unknown parameter '{$k}' in config.");
            }
        }
        return true;
    }

    private function _validateConfigParameters()
    {
        foreach ($this->config as $k => $v) {
            if (is_null($v) && $k !== "accessToken" && $k !== "refreshToken") {
                $this->_logAndThrow("Missing required parameter '{$k}'.");
            }
            switch ($k) {
                case "clientId":
                    if (!preg_match("/^amzn1\.application-oa2-client\.[a-z0-9]{32}$/i", $v)) {
                        $this->_logAndThrow("Invalid parameter value for clientId.");
                    }
                    break;
                case "clientSecret":
                    if (!preg_match("/^[a-z0-9]{64}$/i", $v)) {
                        $this->_logAndThrow("Invalid parameter value for clientSecret.");
                    }
                    break;
                case "accessToken":
                    if (!is_null($v)) {
                        if (!preg_match("/^Atza(\||%7C|%7c).*$/", $v)) {
                            $this->_logAndThrow("Invalid parameter value for accessToken.");
                        }
                    }
                    break;
                case "refreshToken":
                    if (!is_null($v)) {
                        if (!preg_match("/^Atzr(\||%7C|%7c).*$/", $v)) {
                            $this->_logAndThrow("Invalid parameter value for refreshToken.");
                        }
                    }
                    break;
                case "sandbox":
                    if (!is_bool($v)) {
                        $this->_logAndThrow("Invalid parameter value for sandbox.");
                    }
                    break;
            }
        }
        return true;
    }

    private function _setEndpoints()
    {
        /* check if region exists and set api/token endpoints */
        if (array_key_exists(strtolower($this->config["region"]), $this->endpoints)) {
            $region_code = strtolower($this->config["region"]);
            if ($this->config["sandbox"]) {
                $this->endpoint = "https://{$this->endpoints[$region_code]["sandbox"]}/{$this->apiVersion}";
            } else {
                $this->endpoint = "https://{$this->endpoints[$region_code]["prod"]}/{$this->apiVersion}";
            }
            $this->tokenUrl = $this->endpoints[$region_code]["tokenUrl"];
        } else {
            $this->_logAndThrow("Invalid region.");
        }
        return true;
    }

    private function _logAndThrow($message)
    {
        Log::info($message);
        throw new \InvalidArgumentException($message);
    }

}